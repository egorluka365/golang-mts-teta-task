package main

import (
	"fmt"
)

func getLetterCount(str string) string {
	charCount := 0
	lastChar := ' '
	var result string

	for _, char := range str {
		if char == lastChar {
			charCount++
		} else {
			if lastChar != ' ' {
				result += fmt.Sprint(string(lastChar), charCount)
			}
			lastChar = char

			charCount = 1
		}
	}
	result += fmt.Sprint(string(lastChar), charCount)
	return result
}

func main() {
	str := "aaabbbcccc"

	result := getLetterCount(str)
	fmt.Println(result)
}
